﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace xmlReplacer
{
    public partial class Form1 : Form
    {
        private string path1;
        private string path2;
        private string targetNode="//shop/offers/offer";
        private string innerNodeTemplate = "description";
        private string nodeAttribute = "id";

        private XmlDocument doc1;
        private XmlDocument doc2;

        public Form1()
        {
            InitializeComponent();
        }

        public XmlDocument OpenXml(string path)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(path);
                return doc;
            }
            catch (Exception e)
            {
                MessageBox.Show($"Ошибка чтения: {e.Message}");
                return null;
            }
        }


        public void UpdateProgressInvoke(XmlWorkStat workStat)
        {
            progress.Invoke(new Action(() => 
            {
                progress.Text = workStat.ToString();
            }));
        }


        public void ProcessXml(XmlDocument document1, XmlDocument document2)
        {
            XmlElement root1 = document1.DocumentElement;
            XmlElement root2 = document2.DocumentElement;

            XmlNodeList nodes1 = root1.SelectNodes(targetNode);
            XmlNodeList nodes2 = root2.SelectNodes(targetNode);

            var xws = new XmlWorkStat
            {
                Total = nodes1.Count,
                TotalOther = nodes2.Count
            };

            nodes1.Cast<XmlNode>().AsParallel().ForAll(node1 => 
            {
                xws.Processed++;
                UpdateProgressInvoke(xws);
                try
                {
                    var attrVal = node1.SelectSingleNode($"@{nodeAttribute}").InnerText;
                    var innerNode = node1.SelectSingleNode(innerNodeTemplate);

                    if (innerNode == null || string.IsNullOrEmpty(innerNode.InnerText))
                    {
                        xws.EmptySrcNode++;
                        return;
                    }


                    var otherNode = root2.SelectSingleNode($"{targetNode}[@{nodeAttribute}={attrVal}]")?.SelectSingleNode(innerNodeTemplate);

                    if (otherNode == null || string.IsNullOrEmpty(otherNode.InnerText))
                    {
                        xws.EmptyOtherNode++;
                        return;
                    }


                    if (!string.Equals(innerNode.InnerText, otherNode.InnerText))
                    {
                        xws.Diff++;
                        innerNode.InnerText += $", {otherNode.InnerText}";
                    }
                    else
                    {
                        xws.Same++;
                    }

                }
                catch (Exception e)
                {
                    xws.Errors++;
                }
            });
            //Parallel.ForEach(nodes1.Cast<XmlNode>(), (XmlNode node1) =>
            //{
            //    xws.Processed++;
            //    UpdateProgressInvoke(xws);
            //    try
            //    {
            //        var attrVal = node1.SelectSingleNode($"@{nodeAttribute}").InnerText;
            //        var innerNode = node1.SelectSingleNode(innerNodeTemplate);

            //        if (innerNode == null || string.IsNullOrEmpty(innerNode.InnerText))
            //        {
            //            xws.EmptySrcNode++;
            //            return;
            //        }


            //        var otherNode = root2.SelectSingleNode($"{targetNode}[@{nodeAttribute}={attrVal}]")?.SelectSingleNode(innerNodeTemplate);

            //        if (otherNode == null || string.IsNullOrEmpty(otherNode.InnerText))
            //        {
            //            xws.EmptyOtherNode++;
            //            return;
            //        }


            //        if (!string.Equals(innerNode.InnerText, otherNode.InnerText))
            //        {
            //            xws.Diff++;
            //            innerNode.InnerText += $", {otherNode.InnerText}";
            //        }
            //        else
            //        {
            //            xws.Same++;
            //        }

            //    }
            //    catch (Exception e)
            //    {
            //        xws.Errors++;
            //    }
            //});
        }
        //
        public void UnlockButtons(bool isLock)
        {
            button1.Enabled = isLock;
            button2.Enabled = isLock;
        }

        public void ResetUi()
        {
            button1.Enabled = false;
            button2.Enabled = false;
            label1.Text = "not selected";
            label2.Text = "not selected";
            progress.Text = "";
            openFileDialog1.Reset();
            openFileDialog2.Reset();
        }

        public void Button1Click(object sender, EventArgs evargs)
        {
            var dialogResult = openFileDialog1.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                doc1 = OpenXml(openFileDialog1.FileName);
                if (doc1 != null)
                {
                    label1.Text = openFileDialog1.FileName;
                    path1 = openFileDialog1.FileName;
                    button2.Enabled = true;
                }
                else
                {
                    openFileDialog1.Reset();
                }             
            }
        }
        //
        public async void Button2Click(object sender, EventArgs evargs)
        {
            var dialogResult = openFileDialog2.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                if (openFileDialog1.FileName == openFileDialog2.FileName)
                {
                    MessageBox.Show($"Ошибка: одинаковые файлы");
                    return;
                }

                doc2 = OpenXml(openFileDialog2.FileName);
                if (doc2 != null)
                {
                    label2.Text = openFileDialog2.FileName;
                    path2 = openFileDialog2.FileName;
                    UnlockButtons(false);

                    try
                    {
                        await Task.Run(() =>
                        {
                            ProcessXml(doc1, doc2);
                            doc1.Save(path1);
                        });
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show($"Ошибка: не удалось обработать документ {e.Message}");
                        ResetUi();
                        return;
                    }
                    MessageBox.Show($"Оk");
                    UnlockButtons(true);
                }
                else
                {
                    openFileDialog2.Reset();
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            targetNode = boxTarget.Text;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            boxTarget.Text = targetNode; //default
            boxInner.Text = innerNodeTemplate;
            boxAttribute.Text = nodeAttribute;

            textBox1_TextChanged(this, EventArgs.Empty);
            boxInner_TextChanged(this, EventArgs.Empty);
            boxAttribute_TextChanged(this, EventArgs.Empty);
        }

        private void boxInner_TextChanged(object sender, EventArgs e)
        {
            innerNodeTemplate = boxInner.Text;
        }

        private void boxAttribute_TextChanged(object sender, EventArgs e)
        {
            nodeAttribute = boxAttribute.Text;
        }
    }
}
