﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xmlReplacer
{
    public class XmlWorkStat
    {
        public long Total { get; set; } = 0;
        public long TotalOther { get; set; } = 0;
        public long Processed { get; set; } = 0;
        public long Errors { get; set; } = 0;
        public long Same { get; set; } = 0;
        public long Diff { get; set; } = 0;
        public long EmptySrcNode { get; set; } = 0;
        public long EmptyOtherNode { get; set; } = 0;

        public override string ToString()
        {
            return $"{Processed}/{Total}, во втором: {TotalOther}\n" +
                   $"ошибки: {Errors}\n" +
                   $"одинаковые: {Same}\n" +
                   $"разные: {Diff}\n" +
                   $"пустое значение в первом: {EmptySrcNode}\n" +
                   $"не найдено во втором: {EmptyOtherNode}";
        }
    }
}
